<?php

/**
 * Copyright 2020 cpke
 */

require_once(__DIR__ . '/APIError.php');

class AttackalyzerAPI
{
    function __construct(string $api_key = null)
    {
        $this->_api_key = $api_key;
        $this->_url_base = 'https://api.attackalyzer.xyz/api/v1';
        $this->_headers = [
            "Content-Type: application/json",
            "Authorization: Bearer " . $api_key
        ];
        $this->_ch = curl_init();
    }

    /**
     * Execute the cURL request and handle the response
     *
     * @param $curl_handle
     * @return bool|mixed The response, false if not successful
     * @throws Exception
     */
    private function handle_response($curl_handle)
    {
        // Clear previous errors
        $this->_errors = [];

        $response = curl_exec($curl_handle);

        $decoded_response = json_decode($response, true);
        $http_code = curl_getinfo($curl_handle, CURLINFO_HTTP_CODE);

        if (curl_errno($curl_handle)) {
            throw new Exception(curl_error($curl_handle));
        }

        if ($http_code >= 200 && $http_code <= 299) {
            if ($decoded_response && array_key_exists("data", $decoded_response)) {
                return $decoded_response;
            }
            return false;
        } elseif (array_key_exists('errors', $decoded_response)) {
            foreach ($decoded_response['errors'] as $error) {
                $error_obj = new APIError($error['status'], $error['title'], $error['detail']);
                array_push($this->_errors, $error_obj);
            }
            return false;
        } else {
            file_put_contents('php://stderr', "Received a bad response with no 'errors' key: " . print_r($response, true));
            return false;
        }
    }

    /**
     * Attempt to authenticate with the Attackalyzer API using credentials and receive an access token
     *
     * @param string $identifier The identifier which can either be an email or username
     * @param string $password The account's password
     * @return bool|mixed The response, false if not successful
     * @throws Exception
     */
    public function authenticate(string $identifier, string $password)
    {
        curl_setopt($this->_ch, CURLOPT_RETURNTRANSFER, true);

        $identifier_field = (filter_var($identifier, FILTER_VALIDATE_EMAIL) ? "email" : "username");
        $params = [
            'data' => [
                $identifier_field => $identifier,
                'password' => $password
            ]
        ];
        $endpoint = "$this->_url_base/authenticate";
        curl_setopt($this->_ch, CURLOPT_URL, $endpoint);
        curl_setopt($this->_ch, CURLOPT_HTTPHEADER, $this->_headers);
        curl_setopt($this->_ch, CURLOPT_POST, true);
        curl_setopt($this->_ch, CURLOPT_POSTFIELDS, json_encode($params));

        $response = $this->handle_response($this->_ch);

        if ($response !== false) {
            $this->_api_key = $response["data"]["access_token"];
        }

        return $response;
    }

    /**
     * @param string $filename The capture filename to upload and analyze
     * @return bool|mixed false if failure, otherwise the analysis results as an associative array
     * @throws Exception
     */
    public function analyze(string $filename)
    {
        $data = array(
            'capture' => new CurlFile($filename, mime_content_type($filename), $filename)
        );


        curl_setopt($this->_ch, CURLOPT_RETURNTRANSFER, true);

        $headers = [
            'Content-Type: multipart/form-data',
            'Authorization: Bearer ' . $this->_api_key
        ];
        curl_setopt($this->_ch, CURLOPT_HTTPHEADER, $headers);

        $endpoint = "$this->_url_base/analyze";
        curl_setopt($this->_ch, CURLOPT_URL, $endpoint);
        curl_setopt($this->_ch, CURLOPT_POST, true);
        curl_setopt($this->_ch, CURLOPT_POSTFIELDS, $data);

        $response = $this->handle_response($this->_ch);

        return $response ? $response : false;
    }

    /**
     * Retrieve all errors that occurred
     *
     * @return array All errors since the last request
     */
    public function get_errors()
    {
        return $this->_errors;
    }

    private $_api_key;
    private $_url_base;
    private $_errors = [];
    private $_headers = [];
    private $_ch;
}
