<?php

/**
 * Copyright 2020 cpke
 */

class APIError
{
    function __construct($status, $title, $detail)
    {
        $this->_status = $status;
        $this->_title = $title;
        $this->_detail = $detail;
    }

    /**
     * Get the status of the APIError
     *
     * @return mixed
     */
    public function get_status()
    {
        return $this->_status;
    }

    /**
     * Retrieve the title
     *
     * @return mixed
     */
    public function get_title()
    {
        return $this->_title;
    }

    /**
     * Get the error's details
     *
     * @return mixed
     */
    public function get_detail()
    {
        return $this->_detail;
    }

    private $_status;
    private $_title;
    private $_detail;

}