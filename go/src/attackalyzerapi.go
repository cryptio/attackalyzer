// Copyright 2020 cpke

// Package attackalyzerapi implements a client library for Attackalyzer in Go
package attackalyzerapi

import (
	"AttackalyzerAPI/attackalyzerapi/models"
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
)

// Client is used to define an AttackalyzerAPI client
type Client struct {
	apiKey     string
	baseURL    string
	httpClient *http.Client
	apiErrors  []models.Error
}

// NewClient creates a new Attackalyzer API client.
func NewClient() Client {
	return Client{baseURL: "https://api.attackalyzer.xyz/api/v1"}
}

// Errors retrieves all of the API errors reported by the server.
// The data will be reset every time a new function call receives
// 	an unsuccessful response from the server.
func (client *Client) Errors() []models.Error {
	return client.apiErrors
}

// handleResponse handles responses returned from Attackalyzer, ensuring that a successful response has
// 	been returned.
// It returns any error it encounters.
func (client *Client) handleResponse(response *http.Response, unmarshalTo interface{}) error {
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return err
	}
	bodyString := string(body)
	var objMap map[string]*json.RawMessage

	err = json.Unmarshal(body, &objMap)
	if err != nil {
		return err
	}

	if response.StatusCode == http.StatusOK {
		if _, ok := objMap["data"]; ok {
			err = json.Unmarshal(body, &unmarshalTo)
			if err != nil {
				return err
			}
		} else {
			err = errors.New(fmt.Sprintf("Received a successful response with no 'data' key: %s\n", bodyString))
		}
	} else {
		if _, ok := objMap["errors"]; ok {
			var apiErrors []models.Error
			err = json.Unmarshal(*objMap["errors"], &apiErrors)
			if err != nil {
				return err
			}
			err = errors.New(apiErrors[0].Detail)
			client.apiErrors = apiErrors
		} else {
			err = errors.New(fmt.Sprintf("Received a %d response code with no 'data' key: %s\n", response.StatusCode, bodyString))
		}
	}
	return err
}

// Authenticate attempts to authenticate with Attackalyzer's backend using a username and password to obtain an access.
func (client *Client) Authenticate(username, password string) error {
	client.httpClient = &http.Client{}
	endpoint := client.baseURL + "/authenticate"

	// Anonymous struct
	requestData := models.Data{Data: struct {
		Username string `json:"username"`
		Password string `json:"password"`
	}{
		Username: username,
		Password: password,
	}}
	jsonData, err := json.Marshal(requestData)
	if err != nil {
		return err
	}

	req, err := http.NewRequest("POST", endpoint, bytes.NewBuffer(jsonData))
	if err != nil {
		return err
	}
	req.Header.Add("Content-Type", "application/json")

	resp, err := client.httpClient.Do(req)
	if err != nil {
		return err
	}

	var objMap map[string]*json.RawMessage
	err = client.handleResponse(resp, &objMap)
	if err != nil {
		return err
	}

	var token models.Token
	err = json.Unmarshal(*objMap["data"], &token)
	if err != nil {
		return err
	} else {
		client.apiKey = token.AccessToken
	}

	return err
}

// Analyze uploads a packet capture to Attackalyzer which gets analyzed for attacks.
// It returns the analysis results in a map upon success
func (client *Client) Analyze(filename string) (map[string]interface{}, error) {
	client.httpClient = &http.Client{}
	endpoint := client.baseURL + "/analyze"

	file, err := os.Open(filename)

	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	reqBody := &bytes.Buffer{}
	writer := multipart.NewWriter(reqBody)
	const filetype = "application/vnd.tcpdump.pcap"
	part, err := writer.CreateFormFile("capture", filepath.Base(file.Name()))

	if err != nil {
		return nil, err
	}

	// We don't care about how many bytes were copied, explicitly ignore the first value returned
	_, err = io.Copy(part, file)
	if err != nil {
		return nil, err
	}
	_ = writer.Close()
	req, err := http.NewRequest("POST", endpoint, reqBody)

	if err != nil {
		return nil, err
	}

	req.Header.Add("Content-Type", writer.FormDataContentType())
	// We need to use our API key to access this endpoint
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", client.apiKey))

	resp, err := client.httpClient.Do(req)
	if err != nil {
		return nil, err
	}

	var jsonMap map[string]interface{}
	err = client.handleResponse(resp, &jsonMap)

	return jsonMap, err
}
