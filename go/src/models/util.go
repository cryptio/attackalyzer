// Copyright 2020 cpke

// Package models defines structures to hold data to be sent/received to/from Attackalyzer's backend
package models

// Data is a wrapper for all JSON data that gets sent to Attackalyzer
type Data struct {
	Data interface{} `json:"data"`
}

// Error represents an error returned from Attackalyzer
type Error struct {
	Status int    `json:"int"`
	Title  string `json:"title"`
	Detail string `json:"detail"`
}

// Token represents an access token retrieved from Attackalyzer
type Token struct {
	AccessToken string `json:"access_token"`
	TokenType   string `json:"token_type"`
	ExpiresIn   int    `json:"expires_in"`
}
