__author__ = 'cpke'
__copyright__ = 'Copyright 2020, cpke'

__version__ = '0.0.1'
__maintainer__ = 'cpke'
__email__ = 'cryptio@pm.me'
__status__ = 'Production'

import requests
from error import Error


class AttackalyzerAPI:
    def __init__(self, api_key: str = None):
        self.api_key = api_key

    def authenticate(self, username: str, password: str):
        """
        Authenticate with Attackanalyzer's API to receive an access token which will be used in subsequent requests

        :param username: The username to use
        :param password: THe password to use
        :return: A boolean indicating whether or not it succeeded
        """

        # The data to send
        post_data = {
            'data': {
                'username': username,
                'password': password
            }
        }
        response = requests.post(self.base_url + '/authenticate', json=post_data)
        data = self.handle_response(response)
        if not data:
            return False
        self.api_key = data["data"]['access_token']

        return True

    def handle_response(self, response: requests.Response):
        """
        Check the response of a request to the API

        :param response: The response from the request
        :return: Either the data contained within the "data" JSON key if it succeeded, or False
        """

        response_json = response.json()
        if 200 <= response.status_code <= 299:
            if 'data' in response_json:
                return response_json
            else:
                raise RuntimeError("Received a successful response without a \"data\" key")
        else:
            if 'errors' in response_json:
                for error in response_json["errors"]:
                    self.errors.append(Error(
                        error['status'],
                        error['title'],
                        error['detail']
                    ))
                return False
            return False

    def analyze(self, capture_file: str):
        """
        Analyze a packet capture file to find signs of (D)DoS attacks

        :param capture_file: The capture filename to analyze
        :return: Either the data contained within the "data" JSON key if it succeeded, or False
        """

        headers = {
            'Authorization': 'Bearer {}'.format(self.api_key)
        }

        with open(capture_file, 'rb') as f:
            response = requests.post(self.base_url + '/analyze', files={'capture': f}, headers=headers)
            data = self.handle_response(response)
            return data

    errors = []
    base_url = 'https://api.attackalyzer.xyz/api/v1'
    api_key = None
