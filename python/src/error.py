__author__ = 'cpke'
__copyright__ = 'Copyright 2020, cpke'

__version__ = '0.0.1'
__maintainer__ = 'cpke'
__email__ = 'cryptio@pm.me'
__status__ = 'Production'


class Error:
    """
    Represents an error returned from Attackalyzer's backend
    """

    def __init__(self, status: int, title: str, detail: str):
        self.status = status
        self.title = title
        self.detail = detail

    status = None
    title = None
    detail = None
