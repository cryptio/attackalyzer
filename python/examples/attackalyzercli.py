__author__ = 'cpke'
__copyright__ = 'Copyright 2020, cpke'

__version__ = '0.0.1'
__maintainer__ = 'cpke'
__email__ = 'cryptio@pm.me'
__status__ = 'Production'

import argparse
import json
import threading
import itertools
import sys
import time
import getpass
from attackalyzerapi import AttackalyzerAPI

PARSER = argparse.ArgumentParser(description="Analyzes packet captures files to find (D)DoS attacks")
PARSER.add_argument("-f", "--file", help="The packet capture file to read the traffic from", type=str, required=True)

ARGS = PARSER.parse_args()

LOADING_DONE = False


def loading_animation(wait_message=None):
    """
    Displays a loading animation

    :param wait_message: The loading message to show beside the loading animation
    :return:
    """

    for c in itertools.cycle(['|', '/', '-', '\\']):
        if LOADING_DONE:
            break
        sys.stdout.write('\r{} {}'.format(wait_message, c))
        sys.stdout.flush()
        time.sleep(0.1)
    sys.stdout.write('\r')


def demand_credentials():
    """
    Asks the user to enter a username and password

    :return: The credentials entered
    """

    # Emulates a do while loop in Python
    while True:
        username = str(input('[?] Username: '))
        password = getpass.getpass(prompt="[?] Password: ")

        if username and password:
            break

    return username, password


def main():
    global LOADING_DONE

    username, password = demand_credentials()

    attackalyzerapi = AttackalyzerAPI()
    # Authenticate with the API to receive an access token
    authentication_status = attackalyzerapi.authenticate(username, password)
    if not authentication_status:
        exit('[-] Unable to authenticate: {}'.format(
            attackalyzerapi.errors[0].detail if len(attackalyzerapi.errors) > 0 else 'Unknown error')
        )

    t = threading.Thread(target=loading_animation, args=["[*] Analyzing packet capture"])
    t.start()
    analysis_result = attackalyzerapi.analyze(ARGS.file)
    LOADING_DONE = True
    t.join()

    if not analysis_result:
        exit('[-] Unable to analyze capture: {}'.format(
            attackalyzerapi.errors[0].detail if len(attackalyzerapi.errors) > 0 else 'Unknown error')
        )
        
    attacks = analysis_result["data"]
        
    if attacks["attacks"]:
        print('[+] The following possible (D)DoS attacks were found:')
        print(json.dumps(attacks["attacks"], indent=4))
    else:
        print('[-] No (D)DoS attacks found')


if __name__ == '__main__':
    main()
