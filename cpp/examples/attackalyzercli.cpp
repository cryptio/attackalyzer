/**
 * Copyright 2020 cpke
 */

#include <iostream>
#include <map>
#include <any>
#include <climits>
#include <getopt.h>
#include "nlohmann/json.hpp"
#include "attackalyzerapi.h"

using json = nlohmann::json;

/**
 * Prints the program usage
 *
 * @param program_name The program name
 */
void print_usage(const std::string &program_name) {
    std::cout << "Usage: " << program_name << " -f (path to PCAP file)\n";
}

/**
 * Demand a username and password for Attackalyzer
 *
 * @return An std::pair containing the username and password
 */
std::pair<std::string, std::string> demand_credentials() {
    std::string username;
    std::string password;

    do {
        std::cout << "[?] Username: ";
        std::getline(std::cin, username);

        std::cout << "[?] Password: ";
        std::getline(std::cin, password);
    } while (username.empty() || password.empty());

    return {username, password};
}

int main(int argc, char *argv[]) {
    int opt;
    std::string file;

    const std::string program_name = argv[0];

    /*
     * getopt() return value:
     *  If the option takes a value, that value is pointer to the external variable optarg.
     *  ‘-1’ if there are no more options to process.
     *  ‘?’ when there is an unrecognized option and it stores into external variable optopt.
     *  If an option requires a value (such as -f in our example) and no value is given, getopt normally returns ?.
     *  By placing a colon as the first character of the options string, getopt returns: instead of ? when no value is given.
     */

    /* put ':' in the starting of the
     * string so that program can
     * distinguish between '?' and ':'
     */
    while ((opt = getopt(argc, argv, ":f:")) != -1) {
        switch (opt) {
            case 'f':
                // PATH_MAX is the maximum number of chars that can be in a path name
                file = strndup(optarg, strnlen(optarg, PATH_MAX));
                break;
            case ':':
                std::cout << "option needs a value\n";
                break;
            case '?':
            default:
                print_usage(program_name);
                return EXIT_FAILURE;
        }
    }

    if (file.empty()) {
        print_usage(program_name);
        std::cerr << "[-] Required argument -f missing\n";
        return EXIT_FAILURE;
    }

    const std::pair<std::string, std::string> credentials = demand_credentials();
    auto attackalyzerAPI = AttackalyzerAPI();
    const json authentication_status = attackalyzerAPI.authenticate(credentials.first, credentials.second);
    if (authentication_status.is_null()) {
        std::cerr << "[-] Unable to authenticate: ";
        const std::vector<Error> errors = attackalyzerAPI.get_errors();
        if (errors.empty())
            std::cerr << "Unknown error";
        else
            std::cerr << "Error " << errors[0].status << ": " << errors[0].detail;
        std::cerr << '\n';
        return EXIT_FAILURE;
    }

    const json analysis_results = attackalyzerAPI.analyze(file);
    if (analysis_results.is_null()) {
        std::cerr << "[-] Unable to analyze capture: ";
        const std::vector<Error> errors = attackalyzerAPI.get_errors();
        if (errors.empty())
            std::cerr << "Unknown error";
        else
            std::cerr << "Error " << errors[0].status << ": " << errors[0].detail;
        std::cerr << '\n';
        return EXIT_FAILURE;
    } else {
        std::cout << analysis_results.dump(4) << '\n';
    }

    return 0;
}
