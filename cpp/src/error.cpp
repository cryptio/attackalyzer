/**
 * Copyright 2020 cpke
 */

#include "error.h"

void to_json(json &j, const Error &e) {
    j = json{{"status", e.status},
             {"title",  e.title},
             {"detail", e.detail}};

}

void from_json(const json &j, Error &e) {
    j.at("status").get_to(e.status);
    j.at("title").get_to(e.title);
    j.at("detail").get_to(e.detail);
}