/**
 * Copyright 2020 cpke
 */

#include "../include/attackalyzerapi.h"
#include <unordered_map>
#include <iostream>
#include <sys/stat.h>
#include "error.h"
#include "nlohmann/json.hpp"


// For convenience
using json = nlohmann::json;

AttackalyzerAPI::AttackalyzerAPI() {
    ch_ = curl_easy_init();

    curl_easy_setopt(ch_, CURLOPT_WRITEFUNCTION, write_callback);

    // We need to tell the server that we are sending JSON data
    struct curl_slist *chunk = nullptr;
    chunk = curl_slist_append(chunk, "Content-Type: application/json");
    curl_easy_setopt(ch_, CURLOPT_HTTPHEADER, chunk);
}

bool AttackalyzerAPI::authenticate(const std::string &username, const std::string &password) {
    // Clear any previous errors
    errors_.clear();

    const std::unordered_map<std::string, std::unordered_map<std::string, std::string>> data = {
            {"data", std::unordered_map<std::string, std::string>({
                                                                          {"username", username},
                                                                          {"password", password}
                                                                  })}
    };
    const json j_data(data);

    const std::string url = base_url_ + "/authenticate";
    const std::string data_string = j_data.dump();
    curl_easy_setopt(ch_, CURLOPT_URL, url.c_str());
    curl_easy_setopt(ch_, CURLOPT_POST, 1);
    curl_easy_setopt(ch_, CURLOPT_POSTFIELDS, data_string.c_str());

    const auto response = handle_response(ch_);
    if (!response.is_null()) {
        struct curl_slist *chunk = nullptr;
        const std::string authorization_header = "Authorization: Bearer " + response["data"]["access_token"].get<std::string>();
        chunk = curl_slist_append(chunk, authorization_header.c_str());
        curl_easy_setopt(ch_, CURLOPT_HTTPHEADER, chunk);

        return true;
    }
    return false;
}

json AttackalyzerAPI::analyze(const std::string &file) {
    errors_.clear();

    struct stat file_info{};
    FILE *fd;

    fd = fopen(file.c_str(), "rb"); /* open file to upload */
    if (!fd)
        throw std::runtime_error(strerror(errno));

    // Get the file size
    if (fstat(fileno(fd), &file_info) != 0)
        throw std::runtime_error(strerror(errno));

    const std::string url = base_url_ + "/analyze";

    curl_easy_setopt(ch_, CURLOPT_URL, url.c_str());

    // Tell it to upload to the URL
    curl_easy_setopt(ch_, CURLOPT_UPLOAD, 1L);

    // Set where to read from (on Windows you need to use READFUNCTION too)
    curl_easy_setopt(ch_, CURLOPT_READDATA, fd);

    // Give the size of the upload (optional)
    curl_easy_setopt(ch_, CURLOPT_INFILESIZE_LARGE,
                     (curl_off_t) file_info.st_size);

    const auto response = handle_response(ch_);
    return response;
}

json AttackalyzerAPI::handle_response(CURL *ch) {
    std::string rd_buf;
    curl_easy_setopt(ch, CURLOPT_WRITEDATA, &rd_buf);

    CURLcode res = curl_easy_perform(ch);

    if (res == CURLE_OK) {
        long response_code;
        curl_easy_getinfo(ch, CURLINFO_RESPONSE_CODE, &response_code);


        try {
            const auto json_response = json::parse(rd_buf);

            if (response_code >= 200 && response_code <= 299) {
                if (json_response.find("data") != json_response.end()) {
                    return json_response;
                }
            } else {
                if (json_response.find("errors") != json_response.end()) {
                    for (const auto &error: json_response["errors"]) {
                        Error error_obj = error.get<Error>();
                        errors_.push_back(error_obj);
                    }
                } else {
                    throw std::runtime_error("received an erroneous response code without an \"errors\" key");
                }
            }
        } catch (const json::parse_error &e) {
            std::cerr << "Could not parse JSON response : " << e.what() << '\n';
        }

        return json();
    } else {
        throw std::runtime_error("curl_easy_perform() failed: " + std::string(curl_easy_strerror(res)));
    }
}

std::vector<Error> AttackalyzerAPI::get_errors() const {
    return errors_;
}

size_t write_callback(char *contents, size_t size, size_t nmemb, void *userp) {
    ((std::string *) userp)->append((char *) contents, size * nmemb);
    return size * nmemb;
}

AttackalyzerAPI::~AttackalyzerAPI() {
    curl_easy_cleanup(ch_);
    curl_global_cleanup();
}