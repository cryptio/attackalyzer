/**
 * Copyright 2020 cpke
 */

#ifndef ATTACKALYZERAPI_ATTACKALYZERAPI_H
#define ATTACKALYZERAPI_ATTACKALYZERAPI_H

#include <string>
#include <vector>
#include "curl/curl.h"
#include "error.h"

class AttackalyzerAPI {
public:
    AttackalyzerAPI();

    /**
     * Attempt to authenticate with Attackalyzer's API to receive an access token
     *
     * @note This function must be called before making subsequent requests to the back-end
     * @param username The username to authenticate with
     * @param password The password
     * @return A boolean indicating whether or not authentication succeeded
     */
    bool authenticate(const std::string &username, const std::string &password);

    /**
     * Analyze a packet capture file
     *
     * @param The file to analyze
     * @return The analysis result in JSON format if it succeeded
     */
    json analyze(const std::string &file);

    /**
     * Execute a cURL request and handle the response
     *
     * @param ch The cURL handle
     * @return The parsed JSON data if a successful HTTP response code was received
     */
    json handle_response(CURL *ch);

    /**
     * Retrieve a list of errors returned from the back-end. This should be checked after a
     *
     * function call does not succeed
     * @return All errors returned since the last request
     */
    std::vector<Error> get_errors() const;

    ~AttackalyzerAPI();

private:
    CURL *ch_{nullptr};
    std::string base_url_{"https://api.attackalyzer.xyz/api/v1"};
    std::vector<Error> errors_;
};

// Used so we can read the cURL response into a string
size_t write_callback(char *contents, size_t size, size_t nmemb, void *userp);

#endif //ATTACKALYZERAPI_ATTACKALYZERAPI_H
