/**
 * Copyright 2020 cpke
 */

#ifndef ATTACKALYZERAPI_ERROR_H
#define ATTACKALYZERAPI_ERROR_H

#include "nlohmann/json.hpp"
#include <string>

using json = nlohmann::json;

/**
 * Error represents a single error returned from Attackalyzer's API
 */
struct Error {
    unsigned status;
    std::string title;
    std::string detail;
};

/**
 * Serialize an Error object to JSON
 *
 * @param j The JSON object
 * @param e The Error object to serialize into JSON data
 */
void to_json(json &j, const Error &e);

/**
 * Deserialize JSON to an Error object
 *
 * @param j The JSON data
 * @param e The Error object to cast to
 */
void from_json(const json &j, Error &e);

#endif //ATTACKALYZERAPI_ERROR_H