# Attackalyzer

![alt text](img/attackalyzer.png "Attackalyzer logo")

The official client libraries for Attackalyzer

## Documentation
For information on how to use these Attackalyzer's API and its associated libraries, please refer to [Attackalyzer's docs](https://attackalyzer.xyz/docs).

## Coverage
Coverage for the following languages is listed below. If you would like to contribute language bindings for a programming language that has not been covered, please open a pull request.

| Language | Coverage |
| ------ | ------ |
| Python | 100% |
| C++ | 100% |
| PHP | 100% |
| Go  | 100% |
